#pragma once

#include <functional>

namespace mtf::tp {

using Task = std::function<void()>;

}  // namespace mtf::tp
